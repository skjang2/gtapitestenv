#!/bin/bash

THISDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

TARGET_SERVER=
TARGET_CASE=
DEFAULT_CASE="serial"

usage()
{
    echo "OPTIONS : "
    echo "       -s 	| --server                : Target Server (E1/E2/E4/LOCAL)"
    echo "       -c 	| --case                  : Specify the test case"
    echo "       -p 	| --pip                   : install python package"
}

checkServer()
{
  if [ "$TARGET_SERVER" = "E1" ] || 
     [ "$TARGET_SERVER" = "E2" ] ||
     [ "$TARGET_SERVER" = "E4" ] ||
     [ "$TARGET_SERVER" = "LOCAL" ] ; then
     echo "Target Server : ${TARGET_SERVER}"
  else
	  echo "Unknown Server : ${TARGET_SERVER}"
    exit 1
  fi
}

makerequirement()
{
for d in ${THISDIR}/API/*; do
  if [ -d "$d" ]; then
    cd ${d}

    if [ -e ./requirements.txt ]; then
      rm -rf ./requirements.txt
    fi

    pipreqs ./
    pip install -r requirements.txt
    cd ${THISDIR}
  fi
done

for d in ${THISDIR}/Case/*; do
  if [ -d "$d" ]; then
    cd ${d}

    if [ -e ./requirements.txt ]; then
      rm -rf ./requirements.txt
    fi    

    pipreqs ./
    pip install -r requirements.txt
    cd ${THISDIR}
  fi
done


for d in ${THISDIR}/Library/*; do
  if [ -d "$d" ]; then
    cd ${d}

    #if [ -e ./requirements.txt ]; then
    #  rm -rf ./requirements.txt
    #fi  
    #pipreqs ./
    
    pip install -r ./requirements.txt
    cd ${THISDIR}
  fi
done

}

while [ "$1" != "" ]; do
    case $1 in
        -s | --server )         shift
                                TARGET_SERVER=$1
                                ;;
        -c | --case )           shift
                                TARGET_CASE=$1
                                ;;
        -p | --pip )            shift
                                makerequirement
                                ;;                                   
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

checkServer

if [ -n "${TARGET_CASE}" ]; then
  cd /home/APITestEnv/Case/${TARGET_CASE}
else
  cd /home/APITestEnv/Case/${}EFAULT_CASE
fi

python ./run.py $TARGET_SERVER