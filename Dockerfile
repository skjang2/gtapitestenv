FROM ubuntu:18.04

RUN rm -rf /var/lib/apt/lists/* && \
    sed -i 's/archive.ubuntu.com/ftp.daum.net/g' /etc/apt/sources.list && \
    apt-get update && apt-get install -y \
    python-pip python-dev build-essential wget unzip git-core

RUN pip install pipreqs

WORKDIR "/home/APITestEnv"

