#!/bin/bash

if [[ "$(docker images -q gt/apitestenv 2> /dev/null)" == "" ]]; then
  docker build --tag gt/apitestenv ./
fi

docker run -i -t --rm \
  -v "$(pwd)":/home/APITestEnv \
  gt/apitestenv:latest \

