#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import json
import requests

CUR_DIR = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(CUR_DIR, "../../Library/gtResponse"))
import gtResponse
sys.path.append(os.path.join(CUR_DIR, "../../Library/gtRequest"))
import gtRequest
sys.path.append(os.path.join(CUR_DIR, "../../Library/gtEnv"))
import gtEnv

# Prerequisite
# userId : sxm_5NPE34AF2GT000003@mailinator.com (temporary : M175@yopmail.com)
# pwd : ? (temporary : test1234)
# vin : 5NPE34AF2GT000003
# pin : ?
# enroll status : ACTIVE
# Provisioning 

# Request Token
url = gtEnv.getServerURL(sys.argv[1], 'OBS')
url += "ac/v2/ct/request/cc/login"

payload = "{\n   \"username\":\"TM175@yopmail.com\",\n \
                  \"password\":\"test1234\"\n}"
headers = {
  'client_secret': 'oQ2w7D8Dhu3u9/DRpV1XDA==',
  'client_id': '94ee7bb5c0a289d6f09c9310ca141926',
  'Content-Type': 'application/json'
}

try:
  gtRequest.printRequest("POST", url, headers, payload)  
  response = requests.request("POST", url, headers=headers, data = payload)
except:
  exit(100)

gtResponse.printResponse(response)
gtResponse.checkStatusCode(response.status_code, response.reason)

token = json.loads(response.text.encode('utf8'))

# [ESB-00088] Remote Horn
# EIF API ID : AI_WC_HAL_HORN_LIGHTS_UPLIFT_001
url = gtEnv.getServerURL(sys.argv[1], 'OBS')
url += "ac/v2/ts/remote/rhl/hnl"

payload  = {}
headers = {
  'client_id': '94ee7bb5c0a289d6f09c9310ca141926',
  'content-type': 'application/json',
  'from': 'CWP',
  'language': '0',
  'offset': '-8',
  'to': 'ISS',
  'vin': '5NPE34AF2GT000003',
  'appcloud-vin': 'KMHC05LC1LU142219',
  'gen': '1',
  'registrationId': '1',
  'username': 'TM175@yopmail.com',
  'bluelinkServicePin': '1234',
  'userId': 'TM175@yopmail.com',
  'ccAgentId': 'marc.shin@hyundai-hata.com',
  'username': 'TM175@yopmail.com'
}

headers['access_token'] = token['access_token']

try:
  gtRequest.printRequest("POST", url, headers, payload)  
  response = requests.request("POST", url, headers=headers, data = payload)
except:
  exit(100)
  
if len(response.text.encode('utf-8')) != 0:
  gtResponse.printResponse(response)
  exit(100)
  
gtResponse.checkStatusCode(response.status_code, response.reason)
