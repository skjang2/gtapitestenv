#!/usr/bin/env python

import os
import sys
import json
import requests

CUR_DIR = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(CUR_DIR, "../../Library/gtResponse"))
import gtResponse
sys.path.append(os.path.join(CUR_DIR, "../../Library/gtRequest"))
import gtRequest
sys.path.append(os.path.join(CUR_DIR, "../../Library/gtEnv"))
import gtEnv

url = "https://gateway-uat.verisk.io/token"

payload = "grant_type=client_credentials"
headers = {
  'Content-Type': 'application/x-www-form-urlencoded',
  'Authorization': 'Basic MG9hNGEwNDY2MjFaV2VWbVcycDc6VERFVUs0S1dMcTZnUk9pUTB2dEZZU2ZIQTkzUjNnY0V5UF9lM29jLQ=='
}

try:
  gtRequest.printRequest("POST", url, headers, payload)
  response = requests.request("POST", url, headers=headers, data = payload)
except:
  exit(100)

gtResponse.printResponse(response)
gtResponse.checkStatusCode(response.status_code, response.reason)

token = json.loads(response.text.encode('utf8'))

url = gtEnv.getServerURL(sys.argv[1], 'EIF')
url += "verisk/driverFeedback/hyundai_us/51182?skip=0&take=23"

payload = {}
headers = {
  'Content-Type': 'application/json',
  'Authorization': ''
}
headers['Authorization'] = token['token_type']
headers['Authorization'] += ' ' + token['access_token']

try:
  gtRequest.printRequest("GET", url, headers, payload)
  response = requests.request("GET", url, headers=headers, data = payload)
except:
  exit(100)

gtResponse.printResponse(response)
gtResponse.checkStatusCode(response.status_code, response.reason)
