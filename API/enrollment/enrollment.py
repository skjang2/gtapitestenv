#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import json
import requests

CUR_DIR = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(CUR_DIR, "../../Library/gtResponse"))
import gtResponse
sys.path.append(os.path.join(CUR_DIR, "../../Library/gtRequest"))
import gtRequest
sys.path.append(os.path.join(CUR_DIR, "../../Library/gtEnv"))
import gtEnv

# Prerequisite
# userId : TM175@yopmail.com
# pwd : test1234)
# vin : KMHGN4JE0JT090117
# pin : 1234

# Request Token
url = gtEnv.getServerURL(sys.argv[1], 'OBS')
url += "ac/v2/ct/request/cc/login"

payload = "{\n   \"username\":\"TM175@yopmail.com\",\n \
                  \"password\":\"test1234\"\n}"
headers = {
  'client_secret': 'oQ2w7D8Dhu3u9/DRpV1XDA==',
  'client_id': '94ee7bb5c0a289d6f09c9310ca141926',
  'Content-Type': 'application/json'
}

try:
  gtRequest.printRequest("POST", url, headers, payload)  
  response = requests.request("POST", url, headers=headers, data = payload)
except:
  exit(100)

gtResponse.printResponse(response)
gtResponse.checkStatusCode(response.status_code, response.reason)

token = json.loads(response.text.encode('utf8'))

#[ESB-00125] Vehicle Clear
url = gtEnv.getServerURL(sys.argv[1], 'OBS')
url += "ac/v2/ps/vm/vehicle/clear"

payload  = {}
headers = {
  'Content-Type': 'application/json',
  'client_id': '94ee7bb5c0a289d6f09c9310ca141926',
  'vin': 'KMHGN4JE0JT090117',
  'registrationId': 'H00000282110V5NMS53AA3KH000175',
  'gen': '2',
  'impersonatedUserId': 'TM175@yopmail.com',
  'ccAgentId': 'marc.shin@hyundai-hata.com'
}

headers['access_token'] = token['access_token']

try:
  gtRequest.printRequest("POST", url, headers, payload)  
  response = requests.request("POST", url, headers=headers, data = payload)
except:
  exit(100)

gtResponse.printResponse(response)
#gtResponse.checkStatusCode(response.status_code, response.reason)  

# [ESB-00060] Register a vehicle to an account
url = gtEnv.getServerURL(sys.argv[1], 'OBS')
url += "ac/v2/vh/vm/manage/vehicle/add"

payload = "{ \
   \"assistedDealerCode\":\"CA376\", \
   \"overrideFlag\":false, \
   \"ownerType\":\"OWNER\", \
   \"preferredDealerCode\":\"CA376\", \
   \"userInfo\":{ \
      \"address\":[ \
         { \
            \"additional\":\"\", \
            \"city\":\"FOUNTAIN VALLEY\", \
            \"postalCode\":\"92708\", \
            \"region\":\"CA\", \
            \"street\":\"10550 TALBERT AVENUE\", \
            \"type\":\"PRIMARY\" \
         } \
      ], \
      \"contact\":{ \
         \"phones\":[ \
            { \
               \"fccOptIn\":\"YES\", \
               \"number\":\"3107793481\", \
               \"primaryPhoneIndicator\":\"Yes\", \
               \"type\":\"Mobile\" \
            } \
         ] \
      }, \
      \"firstName\":\"MyH UBI First Name\", \
      \"lastName\":\"MyH UBI Last Name\", \
      \"notificationEmail\":\"TM175@yopmail.com\", \
      \"password\":\"\", \
      \"userId\":\"TM175@yopmail.com\" \
   }, \
   \"vehicle\":{ \
      \"drivingPattern\":\"NORMAL\", \
      \"isActive\":true, \
      \"modelId\":1410, \
      \"modelName\":\"GENESIS G80\", \
      \"nickName\":\"GENESIS G80\", \
      \"odometer\":10, \
      \"odometerUpdatedDate\":\"2020-03-12T01:55:00.0000824-07:00\", \
      \"oldOwner\":\"\", \
      \"regId\":\"\", \
      \"vin\":\"KMHGN4JE0JT090117\", \
      \"year\":\"2018\" \
   } \
}"

headers = {
  'Content-Type': 'application/json',
  'client_id': '94ee7bb5c0a289d6f09c9310ca141926',
  'client_secret': 'oQ2w7D8Dhu3u9/DRpV1XDA==',
  'from': 'CWP',
  'language': '0',
  'offset': '-8',
  'transaction_id': '76678686678_G',
}

headers['access_token'] = token['access_token']

try:
  gtRequest.printRequest("POST", url, headers, payload)  
  response = requests.request("POST", url, headers=headers, data = payload)
except:
  #exit(100)
  print("#######")

gtResponse.printResponse(response)
gtResponse.checkStatusCode(response.status_code, response.reason)  

# [ESB-00121] User Update
url = gtEnv.getServerURL(sys.argv[1], 'OBS')
url += "ac/v2/ct/cm/update"

payload = "{ \
   \"userInfo\":{ \
      \"userId\":\"TM175@yopmail.com\", \
      \"notificationEmail\":\"TM175@yopmail.com\", \
      \"firstName\":\"MyH UBI First Name\", \
      \"lastName\":\"MyH UBI Last Name\", \
      \"middleInitial\":\"\", \
      \"prefix\":\"\", \
      \"suffix\":\"\", \
      \"contact\":{ \
         \"phones\":[ \
            { \
               \"number\":\"3107793481\", \
               \"primaryPhoneIndicator\":\"Yes\", \
               \"fccOptIn\":\"Yes\", \
               \"type\":\"MOBILE\" \
            }, \
            { \
               \"number\":\"7149654856\", \
               \"primaryPhoneIndicator\":\"No\", \
               \"fccOptIn\":\"Yes\", \
               \"type\":\"LANDLINE\" \
            }, \
            { \
               \"number\":\"7764531328\", \
               \"primaryPhoneIndicator\":\"No\", \
               \"fccOptIn\":\"Yes\", \
               \"type\":\"MOBILE\" \
            } \
         ] \
      }, \
      \"securityAnswers\":[ \
      ], \
      \"address\":[ \
         { \
            \"type\":\"PRIMARY\", \
            \"street\":\"10550 Talbert Avenue\", \
            \"additional\":\"\", \
            \"city\":\"Fountain Valley\", \
            \"region\":\"CA\", \
            \"postalCode\":\"92708\" \
         } \
      ], \
      \"pin\":\"1234\" \
   }, \
   \"emergencyContacts\":[ \
   ], \
   \"updateEmergenyContact\":false, \
   \"updateBilling\":true, \
   \"billingIntegrationId\":\"\" \
}"

headers = {
  'content-type': 'application/json',
  'from': 'CWP',
  'language': '0',
  'offset': '-8',
  'client_id': '94ee7bb5c0a289d6f09c9310ca141926',
  'access_token': 'w6q2T/okx5dKu0YxjaGYtZnseBROS8uAOpspFLtX/rnhBTa+yXu6XvpPsTpJJR+73JKBamye1MTkiIklLwPAhrpdiXFrEnXWjm+rCf85wjsssQKbY3GG9nCY7FgxX86e2EW1HkdctIJ9cgj/p4YSCSBnoF7w/1rWAf5mlvJDfoEE2rRV9zmIlq+PtjJZ9IWbPwOWiJF5TugyxPDj6b59Use8yBzUEQFH0Al5DbZZBMYSFv/04ahQAUc2+QDLqiHN6P8Rc6/6kzbt9aG/a6PEetZwdhSlMycdGiCeLGYx8DrUWPmXJGD6DYImHhnhxHehL63kFgL6sCykZ/KTULiEiiL2gFqgQgBJgzR34M+WGFOXmFwjjlzhRNMcoEtYoamtM8NKHzsi7wLAmnyuEXBlbaPDFszAva6iY6O5OzpjAAbbYIDoCnsBwtOC0G0FvhB+xKJ5syiwHIW4REGsgg2s/S6Ojpaoi25cXxQug0gWQb6i0CGsRaI/swy+rCEYsCQjXgldhNnrMZM1JDs24/dsfuEj2PqD5/BsdsSdKsqTTr9us17diWxMb/kLX4MTF7N/2jxI2L5YWByJ0YxNKc9k9sbCL9ngDR9vtg+8nluhAmvaXxtxUQy3Zy3K8EUZirUH3bF83LmqyN42MjvKmXq6f3mbNSVgFeEy8Iep8YL0aHqhDwtkER3vNEN2MFwleDSrmaEgO4HDHZJnkgsvN2YPeysJ3R3fzrbfQ1MShNDeGRk++X/jLsybGYXjdV4jB028',
  'transaction_id': '76678686678_G',
  'client_secret': 'oQ2w7D8Dhu3u9/DRpV1XDA==',
  'Accept': '*/*'
}

headers['access_token'] = token['access_token']

try:
  gtRequest.printRequest("POST", url, headers, payload)  
  response = requests.request("POST", url, headers=headers, data = payload)
except:
  exit(100)
 
gtResponse.printResponse(response)
gtResponse.checkStatusCode(response.status_code, response.reason) 

#[ESB-00044] Product Selector
url = gtEnv.getServerURL(sys.argv[1], 'OBS')
url += "ac/v2/ps/provisioning/products/selector"

payload = "{\"list\":[]}"
headers = {
  'content-type': 'application/json',
  'client_id': '94ee7bb5c0a289d6f09c9310ca141926',
  'vin': 'KMHGN4JE0JT090117',
  'accountIntegrationId': 'OKD8DXO9',
  'productType': 'Y'
}

headers['access_token'] = token['access_token']

try:
  gtRequest.printRequest("POST", url, headers, payload)  
  response = requests.request("POST", url, headers=headers, data = payload)
except:
  exit(100)
  
gtResponse.printResponse(response)
gtResponse.checkStatusCode(response.status_code, response.reason)  

#Bluelink Enrollment
url = gtEnv.getServerURL(sys.argv[1], 'OBS')
url += "ac/v2/ps/provisioning/enroll"

payload = "{ \
   \"tncId\":\"23\", \
   \"paymentRefId\":\"\", \
   \"promoCode\":\"\", \
   \"productGroups\":[ \
      { \
         \"groupDescription\":\"Connected Care Package\", \
         \"groupName\":\"Connected Care\", \
         \"products\":[ \
            { \
               \"creditCardRequired\":false, \
               \"discountedPrice\":0, \
               \"listPrice\":0, \
               \"offerExpirationDate\":\"12/12/2020\", \
               \"productDescription\":\"Activate Standard Term\", \
               \"productName\":\"Connected Care Trial Staging\", \
               \"termUnit\":\"Months\", \
               \"totalTax\":0, \
               \"subscriptionStartDate\":\"03/12/2020\", \
               \"subscriptionEndDate\":\"4/1/2021\", \
               \"productId\":\"1-DMAY0\", \
               \"term\":12, \
               \"endDate\":\"4/1/2021\", \
               \"services\":[ \
                  { \
                     \"paramCategory\":\"SERVICE\", \
                     \"paramId\":\"ACN\" \
                  }, \
                  { \
                     \"paramCategory\":\"SERVICE\", \
                     \"paramId\":\"DTC\" \
                  }, \
                  { \
                     \"paramCategory\":\"Download App\", \
                     \"paramId\":\"ADC.Service\" \
                  }, \
                  { \
                     \"paramCategory\":\"SERVICE\", \
                     \"paramId\":\"RSA\" \
                  }, \
                  { \
                     \"paramCategory\":\"SERVICE\", \
                     \"paramId\":\"MAN\" \
                  }, \
                  { \
                     \"paramCategory\":\"SERVICE\", \
                     \"paramId\":\"IMAT.value.3750\" \
                  }, \
                  { \
                     \"paramCategory\":\"SERVICE\", \
                     \"paramId\":\"IMAT.unit\" \
                  }, \
                  { \
                     \"paramCategory\":\"SERVICE\", \
                     \"paramId\":\"MIT.value.7500\" \
                  }, \
                  { \
                     \"paramCategory\":\"SERVICE\", \
                     \"paramId\":\"MIT.unit\" \
                  }, \
                  { \
                     \"paramCategory\":\"SERVICE\", \
                     \"paramId\":\"SVD\" \
                  }, \
                  { \
                     \"paramCategory\":\"SERVICE\", \
                     \"paramId\":\"SVDDay\" \
                  }, \
                  { \
                     \"paramCategory\":\"SERVICE\", \
                     \"paramId\":\"ODD\" \
                  }, \
                  { \
                     \"paramCategory\":\"SERVICE\", \
                     \"paramId\":\"SLS\" \
                  }, \
                  { \
                     \"paramCategory\":\"SERVICE\", \
                     \"paramId\":\"SOS\" \
                  } \
               ] \
            } \
         ], \
         \"$$hashkey\":\"02T\" \
      }, \
      { \
         \"groupDescription\":\"Guidance Package\", \
         \"groupName\":\"Guidance\", \
         \"products\":[ \
            { \
               \"creditCardRequired\":false, \
               \"discountedPrice\":0, \
               \"listPrice\":0, \
               \"offerExpirationDate\":\"12/12/2020\", \
               \"productDescription\":\"Activate Standard Term\", \
               \"productName\":\"Guidance Trial Staging\", \
               \"termUnit\":\"Months\", \
               \"totalTax\":0, \
               \"subscriptionStartDate\":\"03/12/2020\", \
               \"subscriptionEndDate\":\"4/1/2021\", \
               \"productId\":\"1-DMB2P\", \
               \"term\":12, \
               \"taxList\":\"\", \
               \"endDate\":\"4/1/2021\", \
               \"services\":[ \
                  { \
                     \"paramCategory\":\"SERVICE\", \
                     \"paramId\":\"VRS\" \
                  }, \
                  { \
                     \"paramCategory\":\"SERVICE\", \
                     \"paramId\":\"STC\" \
                  }, \
                  { \
                     \"paramCategory\":\"SERVICE\", \
                     \"paramId\":\"LOS\" \
                  } \
               ] \
            } \
         ], \
         \"$$hashkey\":\"02X\" \
      }, \
      { \
         \"groupDescription\":\"Remote Package\", \
         \"groupName\":\"Remote\", \
         \"products\":[ \
            { \
               \"creditCardRequired\":false, \
               \"discountedPrice\":0, \
               \"listPrice\":0, \
               \"offerExpirationDate\":\"12/12/2020\", \
               \"productDescription\":\"Activate Standard Term\", \
               \"productName\":\"Remote Trial Staging\", \
               \"termUnit\":\"Months\", \
               \"totalTax\":0, \
               \"subscriptionStartDate\":\"03/12/2020\", \
               \"subscriptionEndDate\":\"4/1/2021\", \
               \"productId\":\"1-DMB7M\", \
               \"term\":12, \
               \"endDate\":\"4/1/2021\", \
               \"services\":[ \
                  { \
                     \"paramCategory\":\"SERVICE\", \
                     \"paramId\":\"SVN\" \
                  }, \
                  { \
                     \"paramCategory\":\"SERVICE\", \
                     \"paramId\":\"RFC\" \
                  }, \
                  { \
                     \"paramCategory\":\"SERVICE\", \
                     \"paramId\":\"PAN\" \
                  }, \
                  { \
                     \"paramCategory\":\"PACKAGE\", \
                     \"paramId\":\"RSP\" \
                  }, \
                  { \
                     \"paramCategory\":\"SERVICE\", \
                     \"paramId\":\"RDO\" \
                  }, \
                  { \
                     \"paramCategory\":\"SERVICE\", \
                     \"paramId\":\"RHL\" \
                  }, \
                  { \
                     \"paramCategory\":\"SERVICE\", \
                     \"paramId\":\"RLO\" \
                  }, \
                  { \
                     \"paramCategory\":\"SERVICE\", \
                     \"paramId\":\"RSC\" \
                  }, \
                  { \
                     \"paramCategory\":\"SERVICE\", \
                     \"paramId\":\"RVS\" \
                  }, \
                  { \
                     \"paramCategory\":\"SERVICE\", \
                     \"paramId\":\"SVR\" \
                  }, \
                  { \
                     \"paramCategory\":\"SERVICE\", \
                     \"paramId\":\"VSI\" \
                  } \
               ] \
            } \
         ], \
         \"$$hashkey\":\"02V\" \
      } \
   ] \
}"
headers = {
  'client_id': '94ee7bb5c0a289d6f09c9310ca141926 ',
  'from': 'CWP',
  'Content-Type': 'application/json',
  'VIN': 'KMHGN4JE0JT090117',
  'Language': '0',
  'Offset': '-8',
  'gen': '1',
  'userId': 'TM175@yopmail.com',
  'registrationID': 'H00000282110V5NMS53AA3KH000175',
  'to': 'ISS',
  'blueLinkServicePin': '1234'
}

headers['access_token'] = token['access_token']

try:
  gtRequest.printRequest("POST", url, headers, payload)  
  response = requests.request("POST", url, headers=headers, data = payload)
except:
  exit(100)

gtResponse.printResponse(response)
gtResponse.checkStatusCode(response.status_code, response.reason)
