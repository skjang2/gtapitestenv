#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import json
import requests

CUR_DIR = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(CUR_DIR, "../../Library/gtResponse"))
import gtResponse
sys.path.append(os.path.join(CUR_DIR, "../../Library/gtRequest"))
import gtRequest
sys.path.append(os.path.join(CUR_DIR, "../../Library/gtEnv"))
import gtEnv

# Request Token
url = gtEnv.getServerURL(sys.argv[1], 'OBS')
url += "ac/v2/ct/request/cc/login"

payload = "{\n   \"username\":\"TM175@yopmail.com\",\n \
                  \"password\":\"test1234\"\n}"
headers = {
  'client_secret': 'oQ2w7D8Dhu3u9/DRpV1XDA==',
  'client_id': '94ee7bb5c0a289d6f09c9310ca141926',
  'Content-Type': 'application/json'
}

try:
  gtRequest.printRequest("POST", url, headers, payload)  
  response = requests.request("POST", url, headers=headers, data = payload)
except:
  exit(100)

gtResponse.printResponse(response)
gtResponse.checkStatusCode(response.status_code, response.reason)

token = json.loads(response.text.encode('utf8'))

# [ESB-00060] Register a vehicle to an account
url = gtEnv.getServerURL(sys.argv[1], 'OBS')
url += "ac/v2/vh/vm/manage/vehicle/add"

payload = "\r\n{\r\n  \"assistedDealerCode\": \"CA376\",\r\n \
                      \"overrideFlag\": false,\r\n  \
                      \"ownerType\": \"OWNER\",\r\n \
                      \"preferredDealerCode\": \"CA376\",\r\n  \
                      \"userInfo\": {\r\n    \"address\": [\r\n      {\r\n        \"additional\": \"\",\r\n        \"city\": \"FOUNTAIN VALLEY\",\r\n        \"postalCode\": \"92708\",\r\n        \"region\": \"CA\",\r\n        \"street\": \"10550 TALBERT AVENUE\",\r\n        \"type\": \"PRIMARY\"\r\n      }\r\n    ],\r\n    \"contact\": {\r\n      \"phones\": [\r\n        {\r\n          \"fccOptIn\": \"No\",\r\n          \"number\": \"9498776763\",\r\n          \"primaryPhoneIndicator\": \"Yes\",\r\n          \"type\": \"Mobile\"\r\n        }\r\n      ]\r\n    },\r\n    \"firstName\": \"John\",\r\n    \"lastName\": \"Doe\",\r\n    \"notificationEmail\": \"test123456@mailinator.com\",\r\n    \"password\": \"password1\",\r\n    \"userId\": \"test123456@mailinator.com\"\r\n  },\r\n  \"vehicle\": {\r\n    \"drivingPattern\": \"NORMAL\",\r\n    \"isActive\": true,\r\n    \"modelId\": 1410,\r\n    \"modelName\": \"GENESIS G80\",\r\n    \"nickName\": \"\",\r\n    \"odometer\": 10,\r\n    \"odometerUpdatedDate\": \"2018-07-27T13:21:34.0000349-07:00\",\r\n    \"oldOwner\": \"\",\r\n    \"regId\": \"\",\r\n    \"vin\": \"KMHGN4JE0JT090117\",\r\n    \"year\": \"2018\"\r\n  }\r\n}\r\n"
headers = {
  'Content-Type': 'application/json',
  'client_id': '94ee7bb5c0a289d6f09c9310ca141926',
  'client_secret': 'oQ2w7D8Dhu3u9/DRpV1XDA==',
  'from': 'CWP',
  'language': '0',
  'offset': '-8',
  'transaction_id': '76678686678_G',
  'access_token': 'w6q2T/okx5dKu0YxjaGYtZnseBROS8uAOpspFLtX/rnhBTa+yXu6XvpPsTpJJR+73JKBamye1MTkiIklLwPAhrpdiXFrEnXWjm+rCf85wjsssQKbY3GG9nCY7FgxX86e2EW1HkdctIJ9cgj/p4YSCSBnoF7w/1rWAf5mlvJDfoEE2rRV9zmIlq+PtjJZ9IWbPwOWiJF5TugyxPDj6b59Use8yBzUEQFH0Al5DbZZBMZenKmvCrOVBAdOZ18ljS636P8Rc6/6kzbt9aG/a6PEetZwdhSlMycdGiCeLGYx8DpxjxIkk3zEtApCtBdYk2XAJWr4TveIBJl/jAXczBJFKMgZizd6o2GpJ0MmCFwFl+8sfRJZc9hP64OQo5cRhITvM8NKHzsi7wLAmnyuEXBlbaPDFszAva6iY6O5OzpjAAb1zcBE2iuHJBSo0ijjkQ56E3GKsuTTiGc1wsl2mAYEk1M1c5AVsSXmwseXgeLbVYw4cFo3T/W+YW8On7MoGnu/XgldhNnrMZM1JDs24/dsfuEj2PqD5/BsdsSdKsqTTr9us17diWxMb/kLX4MTF7N/2jxI2L5YWByJ0YxNKc9k9sbCL9ngDR9vtg+8nluhAmvvodDVFgcMI+j9gkwM7OQt9pB26vh7Ht+v1sifJ8D09Mn0pI+zPX5bYY8yPpCAR4wHQI1ejvLyYjxZNnOcohv5EPhTH1SZZkt1WOQKlf5dA4oRY8NttdXB0LXgNmjAe/gZKx2y9EQ+EPAA/dUdZig4'
}

headers['access_token'] = token['access_token']

try:
  gtRequest.printRequest("POST", url, headers, payload)  
  response = requests.request("POST", url, headers=headers, data = payload)
except:
  exit(100)

# 이미 등록되어 있으면 에러를 반환하고, 성공적으로 등록되었다면 응답값이 없다.
# 그럼으로 응답 값을 검사하지 않고, 해당 과정은 Remove Vehicle을 위한 준비 단계로만 수행한다!

# [ESB-00062] Remove vehicle from account
url = gtEnv.getServerURL(sys.argv[1], 'OBS')
url += "ac/v2/vh/vm/manage/vehicle/remove"

payload = "{\r\n    \"userInfo\" : {\r\n        \"userId\" : \"test123456@mailinator.com\"\r\n    },\r\n    \"vehicle\" :{\r\n        \"vin\" : \"KMHGN4JE0JT090117\",\r\n        \"odometer\" : 10,\r\n        \"regId\" : \"\",\r\n        \"drivingPattern\" : \"NORMAL\",\r\n        \"nickName\" : \"\"\r\n    }\r\n}\r\n"
headers = {
  'Content-Type': 'application/json',
  'client_id': '94ee7bb5c0a289d6f09c9310ca141926',
  'client_secret': 'oQ2w7D8Dhu3u9/DRpV1XDA==',
  'from': 'CC',
  'language': '0',
  'offset': '-8',
  'transaction_id': 'TRID20180612000001',
  'access_token': 'w6q2T/okx5dKu0YxjaGYtZnseBROS8uAOpspFLtX/rnhBTa+yXu6XvpPsTpJJR+73JKBamye1MTkiIklLwPAhrpdiXFrEnXWjm+rCf85wjsssQKbY3GG9nCY7FgxX86e2EW1HkdctIJ9cgj/p4YSCSBnoF7w/1rWAf5mlvJDfoEE2rRV9zmIlq+PtjJZ9IWbPwOWiJF5TugyxPDj6b59Use8yBzUEQFH0Al5DbZZBMZASlUU1t67S0jxCId7Nc9H6P8Rc6/6kzbt9aG/a6PEetZwdhSlMycdGiCeLGYx8DrL0rtMREyk2P3H3j9EfS4SwAfUHhU60Cn+rkipAOgmaaptVyKuX+Gp7LxSnZoheXRPRp0oH/U7f2txQwnDnMTxM8NKHzsi7wLAmnyuEXBlbaPDFszAva6iY6O5OzpjAAa/GmFM+9tgblhCDyWJqwgiqMVlS+++ynxKhH8yoCRfhPCMgHkKq2dfnyRpegPN2I5GxDdNoyFBZXb8cyp9xjZtXgldhNnrMZM1JDs24/dsfuEj2PqD5/BsdsSdKsqTTr9us17diWxMb/kLX4MTF7N/2jxI2L5YWByJ0YxNKc9k9sbCL9ngDR9vtg+8nluhAmtM0XhMSBNSWJRRv48c4aVLFTKpXu/w3eS6q6dlBOplnWxC29Y4LSpXaqp82pliRdSkgwAx5cvQfD99bLCZe+BzCiKaYrv9tt71Vi0RA97zoB9e53wAzCeDmk0zm4rtOrbh1TuRmGWsjv1mHS0fuSS8',
  'x-cuid': 'SSCDUGN3',
  'impersonatedCuid': 'SSCDUGN3'
}

headers['access_token'] = token['access_token']

try:
  gtRequest.printRequest("POST", url, headers, payload)  
  response = requests.request("POST", url, headers=headers, data = payload)
except:
  exit(100)

if len(response.text.encode('utf-8')) != 0:
  gtResponse.printResponse(response)
  exit(100)
  
gtResponse.checkStatusCode(response.status_code, response.reason)

