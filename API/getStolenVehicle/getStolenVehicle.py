#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import json
import requests

CUR_DIR = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(CUR_DIR, "../../Library/gtResponse"))
import gtResponse
sys.path.append(os.path.join(CUR_DIR, "../../Library/gtRequest"))
import gtRequest
sys.path.append(os.path.join(CUR_DIR, "../../Library/gtEnv"))
import gtEnv

# Request Token
url = gtEnv.getServerURL(sys.argv[1], 'OBS')
url += "ac/v2/ct/request/cc/login"

payload = "{\n   \"username\":\"TM175@yopmail.com\",\n \
                  \"password\":\"test1234\"\n}"
headers = {
  'client_secret': 'oQ2w7D8Dhu3u9/DRpV1XDA==',
  'client_id': '94ee7bb5c0a289d6f09c9310ca141926',
  'Content-Type': 'application/json'
}

try:
  gtRequest.printRequest("POST", url, headers, payload)  
  response = requests.request("POST", url, headers=headers, data = payload)
except:
  exit(100)

gtResponse.printResponse(response)
gtResponse.checkStatusCode(response.status_code, response.reason)

token = json.loads(response.text.encode('utf8'))

# [ESB-00097]SVR Status - Get
# EIF API ID : AI_WC_HAL_MONI_SVT_UPLIFT_001
url = gtEnv.getServerURL(sys.argv[1], 'OBS')
url += "ac/v2/ts/svr/getSVRStatus"

payload = {}
headers = {
  'client_Id': '94ee7bb5c0a289d6f09c9310ca141926',
  'content-type': 'application/json',
  'Vin': 'KMHCT5AE1GU248025',
  'gen': '1',
  'registrationId': 'H00000282110V5NMS53AA3KH000175'
}

headers['access_token'] = token['access_token']

try:
  gtRequest.printRequest("GET", url, headers, payload)  
  response = requests.request("GET", url, headers=headers, data = payload)
except:
  exit(100)
  
if len(response.text.encode('utf-8')) != 0:
  gtResponse.printResponse(response)
  exit(100)
  
gtResponse.checkStatusCode(response.status_code, response.reason)

'''
# Get Stolen Vehicle Tracker from SXM
url = "https://rts.sit.siriusxm-cv.net/telematicsservices/v1/vehicles/KMHCT5AE1GU248025/stolen-vehicle-tracker"

payload  = {}
headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': '75b3e49e-ffa4-03cc-16c1-9ef17cc51a2b',
  'x-api-key': 'pDg1YJ6SYU9EsV9rrAG1W5HcBqP0SeC06mI1eF1h',
  'Authorization': 'Bearer 75b3e49e-ffa4-03cc-16c1-9ef17cc51a2b'
}

try:
  gtRequest.printRequest("GET", url, headers, payload)  
  response = requests.request("GET", url, headers=headers, data = payload)
except:
  exit(100)

gtResponse.printResponse(response)
gtResponse.checkStatusCode(response.status_code, response.reason)
'''