#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import json
import requests

CUR_DIR = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(CUR_DIR, "../../Library/gtResponse"))
import gtResponse
sys.path.append(os.path.join(CUR_DIR, "../../Library/gtRequest"))
import gtRequest
sys.path.append(os.path.join(CUR_DIR, "../../Library/gtEnv"))
import gtEnv

# Request Token
url = gtEnv.getServerURL(sys.argv[1], 'OBS')
url += "ac/v2/ct/request/cc/login"

payload = "{\n   \"username\":\"TM175@yopmail.com\",\n \
                  \"password\":\"test1234\"\n}"
headers = {
  'client_secret': 'oQ2w7D8Dhu3u9/DRpV1XDA==',
  'client_id': '94ee7bb5c0a289d6f09c9310ca141926',
  'Content-Type': 'application/json'
}

try:
  gtRequest.printRequest("POST", url, headers, payload)  
  response = requests.request("POST", url, headers=headers, data = payload)
except:
  exit(100)

gtResponse.printResponse(response)
gtResponse.checkStatusCode(response.status_code, response.reason)

token = json.loads(response.text.encode('utf8'))

# [ESB-00185] Get Recall and Maintenance Notification
# TM175@yopmail.com 계정에 vin : KMHCT5AE1GU248025 이 추가 되어 있어야 하고 enroll 되어 있어야 한다
# pin은 1234로 등록 되어 있어야 함
# EIF API ID : AI_WC_HAL_HORN_LIGHTS_UPLIFT_001
url = gtEnv.getServerURL(sys.argv[1], 'OBS')
url += "ac/v2/ts/vh/vehicle/reminders?reminderType=maintenance"

payload = {}
headers = {
  'content-type': 'application/json',
  'client_id': '94ee7bb5c0a289d6f09c9310ca141926',
  'access_token': 'w6q2T/okx5dKu0YxjaGYtZnseBROS8uAOpspFLtX/rnhBTa+yXu6XvpPsTpJJR+73JKBamye1MTkiIklLwPAhrpdiXFrEnXWjm+rCf85wjsssQKbY3GG9nCY7FgxX86e2EW1HkdctIJ9cgj/p4YSCSBnoF7w/1rWAf5mlvJDfoEE2rRV9zmIlq+PtjJZ9IWbPwOWiJF5TugyxPDj6b59Use8yBzUEQFH0Al5DbZZBMZ3ask6FIB6gspNJYbywtXm6P8Rc6/6kzbt9aG/a6PEetZwdhSlMycdGiCeLGYx8Dr6m6iuCSjZ6rWBtatrn8taEIorF+V5m4GQPcHjJ9FM+7kS4IJqM+zscFMQsJ8Wjk/8pnBr4pEoJweOO2LZoQ/lM8NKHzsi7wLAmnyuEXBlbaPDFszAva6iY6O5OzpjAAa72m9PYVhX7mduKmSC+HO99BpCQ6LoUTO/Q4nOJTpHMKR2syULMc84xIMahPmOpWWJLUHyJEyq7eL7KmS98bB+XgldhNnrMZM1JDs24/dsfuEj2PqD5/BsdsSdKsqTTr9us17diWxMb/kLX4MTF7N/2jxI2L5YWByJ0YxNKc9k9sbCL9ngDR9vtg+8nluhAmuQYPZGW+2a5SZLUgYE7lpCenE2r0uVdSMvBxnQdEEJv4X+b+kfKiDFj+pL6PJCp1E4q4yta8Rx3D620cEHSNgYkS7KZsEMMgLp61vyhppYopEk1wjMcqkrnrMHMP5/siVVpJx4S+XK6T66mm8RVjsE',
  'vin': 'KMHCT5AE1GU248025',
  'userId': 'TM175@yopmail.com',
  'from': 'CWP',
  'language': '0',
  'offset': '-8',
  'gen': '1',
  'registrationId': 'H00000282110V5NMS53AA3KH000175'
}

try:
  gtRequest.printRequest("GET", url, headers, payload)  
  response = requests.request("GET", url, headers=headers, data = payload)
except:
  exit(100)
  
if len(response.text.encode('utf-8')) != 0:
  gtResponse.printResponse(response)
  exit(100)
  
gtResponse.checkStatusCode(response.status_code, response.reason)
