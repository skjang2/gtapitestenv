#!/usr/bin/env python

import sys
import json
from pygments import highlight, lexers, formatters

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def checkStatusCode(statusCode, reason):
    if statusCode != 200:
        sys.stderr.write(reason)
        exit(100)

def printResponse(response):
    print(bcolors.HEADER + '\r\nResponse : ' + bcolors.ENDC)
    formatted_json = json.dumps(json.loads(response.text.encode('utf8')), indent=4)
    colorful_json = highlight(unicode(formatted_json, 'UTF-8'), lexers.JsonLexer(), formatters.TerminalFormatter())
    print(colorful_json)