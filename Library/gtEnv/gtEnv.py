#!/usr/bin/env python

import sys
import inspect

def getServerURL(server, app):
    if server == 'LOCAL':
        if app == 'OBS':
            return 'http://docker.for.mac.localhost:8080/ngtobs/'
        elif app == 'EIF':
            return 'http://docker.for.mac.localhost:7111/'        
        elif app == 'EIF':
            sys.stderr.write(inspect.currentframe().f_code.co_name + ' : Unknown App\r\n')
            exit(100)
    elif server == 'E4':
        if app == 'OBS':
            return 'http://10.119.125.48:8011/gtobs/'
        elif app == 'EIF':
            return 'http://10.119.125.48:7111/'
        else:
            sys.stderr.write(inspect.currentframe().f_code.co_name + ' : Unknown App\r\n')
            exit(100)
    elif server == 'E2':
        if app == 'OBS':
            return 'http://10.119.125.49:8011/gtobs/'
        elif app == 'EIF':
            return 'http://10.119.125.36:7111/'        
        else:
            sys.stderr.write(inspect.currentframe().f_code.co_name + ' : Unknown App\r\n')
            exit(100)                           
    else:
        sys.stderr.write(inspect.currentframe().f_code.co_name + ' : Unknown Server\r\n')
        exit(100)
        