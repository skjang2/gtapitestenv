#!/usr/bin/env python

import sys
import json
import pprint
from pygments import highlight, lexers, formatters

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def printRequest(method, url, header, body):
    print(bcolors.HEADER + 'Request : ' + bcolors.ENDC)
    print(bcolors.HEADER + '  Method : ' + bcolors.ENDC + method)
    print(bcolors.HEADER + '  URL : ' + bcolors.ENDC + url)
    
    print (bcolors.HEADER + '  Header : ' + bcolors.ENDC)
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(header)
    
    print (bcolors.HEADER + '  Body : ' + bcolors.ENDC)
    pp.pprint(body)
    