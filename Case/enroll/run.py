#!/usr/bin/env python

import os
import sys
import subprocess

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

CWD = os.getcwd()
currentpath = '../../API'
folder = 'enrollment'

print(bcolors.WARNING + '####################################################' + bcolors.ENDC)
print(bcolors.WARNING + os.path.join(CWD, currentpath, folder) + bcolors.ENDC)
print(bcolors.WARNING + '####################################################' + bcolors.ENDC)

os.chdir(os.path.join(CWD, currentpath, folder))

try:
    subprocess.check_call('python ' + './' + folder + '.py ' + sys.argv[1], shell=True)
except subprocess.CalledProcessError as e:
    print(bcolors.FAIL + 'exit code: {}'.format(e.returncode) + bcolors.ENDC)
except:
    print(bcolors.FAIL + 'Unexpected error' + bcolors.ENDC)
    
os.chdir(os.path.join(CWD, currentpath))

print(bcolors.WARNING + '=======================================================\r\n' + bcolors.ENDC)